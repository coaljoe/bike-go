module main

require (
	github.com/bmizerany/assert v0.0.0-20160611221934-b7ed37b82869 // indirect
	github.com/go-gl/glfw/v3.3/glfw v0.0.0-20200707082815-5321531c36a2
	github.com/hjson/hjson-go v3.1.0+incompatible
	github.com/kr/pretty v0.2.1 // indirect
	kristallos.ga/lib/debug v0.0.0-20210129133423-4e6cf78261ae
	kristallos.ga/lib/pubsub v0.0.0-20210129133423-4e6cf78261ae
	//kristallos.ga/rx v0.0.0-20200513151440-e344509e7f02
	kristallos.ga/rx v0.0.0
	//kristallos.ga/rx v0.0.0-20200513151440-e344509e7f02
	//kristallos.ga/rx v0.0.0-20200513151440-e344509e7f02
	//kristallos.ga/rx/math v0.0.0-20200513151440-e344509e7f02
	kristallos.ga/rx/math v0.0.0
	kristallos.ga/rx/phys v0.0.0
)

//replace kristallos.ga/rx/phys => ./phys

replace kristallos.ga/rx => ../rx_phys

replace kristallos.ga/rx/phys => ../rx_phys/phys

replace kristallos.ga/rx/math => ../rx_phys/math

// XXX fixme?
replace kristallos.ga/rx/transform => ../rx_phys/transform

go 1.16
