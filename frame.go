package main

import (
	"kristallos.ga/rx"
	"kristallos.ga/rx/phys"
	. "kristallos.ga/rx/math"
)

type Frame struct {
	*BikeElem

	// Props
	// Size
	dim Vec3
	mass float64

	// Initial pos
	spawnPos Vec3

	spawned bool
}

func NewFrame(props FrameProps) *Frame {
	println("NewFrame()")

	f := &Frame{
		BikeElem: NewBikeElem(),

		dim: props.dim,
		mass: props.mass,

		// Defaults
		spawnPos: Vec3Zero,
	}
	
	return f
}

func (f *Frame) Init() {
	
	l := f.dim.X()
	frame := rx.GenCubeMeshNode(l, 0.6, 0.2)
	ctx.sce.Add(frame)
	//frame.SetPos(Vec3{l/2., 2, xspawnHeight})
			
	frame.Mesh.SetMaterial(ctx.bikeMat)
		
	frame.AddPhys(f.mass, phys.ShapeTypeBox)
	
		
	frame.Phys.SetColGroup(ColGroupBike)
	//fork.Phys.SetColMask(maskAll ^ 2)
	frame.Phys.SetColMask(ColMaskEverything ^ ColGroupBike)
	//fork.Phys.SetColGroup(0)
	//fork.Phys.SetColMask(0)
	
	//frame.SetVisible(false)
	//fork.Phys.ClearForces()
	frame.Phys.DisableDeactivation()
	//panic(2)

	f.node = frame
}

func (f *Frame) Spawn() {

	f.node.SetPos(f.spawnPos)
	//f.node.Phys.SetPos(f.spawnPos)

	f.spawned = true
}
