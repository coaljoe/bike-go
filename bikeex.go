package main

import (
	"kristallos.ga/rx"
	. "kristallos.ga/rx/math"
)

type BikeElemI interface {
	Node() *rx.Node
	Name() string
}

type BikeElem struct {
	name string
	// Node and PhysObj
	node *rx.Node
}
func (be *BikeElem) Node() *rx.Node { return be.node }
func (be *BikeElem) Name() string { return be.name }

func NewBikeElem() *BikeElem {
	be := &BikeElem{}

	return be
}

func (be *BikeElem) Pos() Vec3 {
	// XXX buggy: there may be desyncs between
	// Phys transform and Node transforms
	return be.node.Pos()
}

func (be *BikeElem) SetPos(p Vec3) {
	be.node.SetPos(p)
}

func (be *BikeElem) Rot() Vec3 {
	return be.node.Rot()
}

func (be *BikeElem) SetRot(r Vec3) {
	be.node.SetRot(r)
}
