package main

import (
	"kristallos.ga/rx"
	"kristallos.ga/rx/phys"
	. "kristallos.ga/rx/math"
)

type Wheel struct {
	*BikeElem

	// Props
	// Size
	diameter float64
	width float64
	mass float64
	friction float64

	// Initial pos
	spawnPos Vec3

	spawned bool
}

//func newWheel(props WheelProps, spawnPos Vec3) *Wheel {
func NewWheel(props WheelProps) *Wheel {
	println("NewWheel()")

	w := &Wheel{
		BikeElem: NewBikeElem(),

		diameter: props.diameter,
		width: props.width,
		mass: props.mass,
		friction: props.friction,

		// Defaults
		spawnPos: Vec3Zero,
	}
	
	return w
}

func (w *Wheel) Init() {
	w1 := rx.GenCylinderMeshNode(w.diameter, w.diameter, w.width, 16)
	
	ctx.sce.Add(w1)
	w1.SetPos(w.spawnPos)
	w1.SetRot(Vec3{-90, 0, 0})
		

	w1.AddPhys(w.mass, phys.ShapeTypeCylinder)

	newRot := w1.Phys.Rot()
	println("newRot:", newRot)
	//panic(2)

	w1.Phys.SetFriction(w.friction)

	w1.Phys.SetColGroup(ColGroupBike)
	//w1.Phys.SetColMask(maskAll ^ 2)
	w1.Phys.SetColMask(ColMaskEverything ^ ColGroupBike)
	//w1.Phys.SetColMask(0xffff)
	//panic(2)

	//w1.SetVisible(false)
	//w1.Phys.ClearForces()
	w1.Phys.DisableDeactivation()

	w.node = w1
}

func (w *Wheel) Spawn() {

	w.spawned = true
}
