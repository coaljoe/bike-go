package main
// 
import (
	"kristallos.ga/rx"
	//"kristallos.ga/rx/phys"
	"kristallos.ga/rx/phys/bt"
	//"kristallos.ga/rx/phys/btcpp"
	"fmt"
	//. "math"

	. "kristallos.ga/rx/math"
)

//type ColGroup uint

const (
	//ColGroupWall ColGroup =  1 << iota // 0th bit (0x1)
	ColGroupWall uint =  1 << iota // 0th bit (0x1)
	ColGroupBike
	ColGroupPowerUp
	ColGroupZZ

	ColGroupNone = 0 // 0 bits set
)

// XXX move to phys?
const ColMaskNothing uint = 0
const ColMaskEverything uint = 0xffffffff

/*
func (c ColGroup) String() string {
	names := []string{"ColGroupNone", "ColGroupBike",
		"ColGroupWall", "ColGroupPowerUp", "ColGroupZZ"}
	idx := uint(c)
	println("idx:", idx)
	//name := names[idx]
	//return name
	_ = names
	return ""
}
*/

func colGroupTest() {
	fmt.Println("colGroupTest()")

	fmt.Printf("%#x\n", ColGroupNone)
	fmt.Printf("%#x\n", ColGroupWall)
	println()
	fmt.Printf("%#08b\n", ColGroupNone)
	fmt.Printf("%#08b\n", ColGroupWall)
	fmt.Printf("%#08b\n", ColGroupBike)
	fmt.Printf("%#08b\n", ColGroupPowerUp)
	println()
	fmt.Printf("%#08b\n", ColGroupWall|ColGroupBike)
	fmt.Printf("%#08b\n", ColGroupWall|ColGroupPowerUp)

	/*
	println()
	fmt.Println("bike:", ColGroupBike)
	fmt.Println("wall:", ColGroupWall)
	fmt.Println("zz:", ColGroupZZ)
	fmt.Println("zz2:", int(ColGroupZZ))
	fmt.Printf("zz3 %#08b\n", ColGroupZZ)
	println("test")	
	*/

	println()

}

func main() {
	fmt.Println("main()")

	colGroupTest()
	//panic(2)

	/*
	app := rx.TestFwInit()
	sce := rx.TestFwCreateDefaultScene()
	_ = sce
	*/

	xconf := rx.NewDefaultConf()
	xconf.ResPath = "res"
	xconf.DebugDraw = true

	//rxi := rx.Init(1024, 576, xconf)
	rxi := rx.Init(1024, 600, xconf)
	app := rxi.App
	sce := rxi.Scene


	// XXX fixme: add sce.SetDefaultScene() to rx?
	l1 := sce.CreateLightNode("light1")
	println("l1 pos:", l1.Pos().String())
	

	initContext()

	sce.Lights()[0].SetPos(Vec3{0, 0, 40})

	// Init physics
	bt.Init()

	_rxi := rx.Rxi()
	_ = _rxi

	ctx.rxi = _rxi
	ctx.sce = sce

	inputsys := _rxi.InputSys
		_ = inputsys

	_rxi.PhysSys.EnableDebugDraw()
	//_rxi.PhysSys.DisableDebugDraw()

	//_rxi.PhysSys.AddFloor()
	_rxi.PhysSys.AddFloorEx(Vec3{0, 0, 1}, 1000)
	
	//_rxi.PhysSys.WorldSetGravity(Vec3{0, 0, -1})

	//panic(2)

	sl := rx.NewSceneLoader()
	_ = sl

	cn := rx.Rxi().Camera

	/*
	// Iso cam
	cn.SetPos(Vec3{12.24745, -12.24745, 10.0})
	// Game iso 1:2
	cn.SetRot(Vec3{60, 0, 45})
	*/

	// Test camp
	cn.SetPos(Vec3{12.24745, -12.24745, 10.0})
	//cn.SetRot(Vec3{-99.53, 183.71, 165.25})
	//cn.SetRot(Vec3{82.5, 1, 15})
	cn.SetRot(Vec3{76.1931, 2.7984, 11.2283}) // plat2.blend

	// Move camera back
	cn.MoveByVec(Vec3{0, 0, 200})
	// XXX prevents some line blinking: fixme?
	cn.Camera.SetZnear(0.01)
	cn.Camera.SetZfar(100000)

	moveCamera := true
	//moveCamera := false
	initialCamPos := cn.Pos()

	/*
	// Test cam
	cn.SetPos(Vec3{0, 0, 50.0})
	// Game iso 1:2
	cn.SetRot(Vec3{0, 0, 0})
	*/

	//cn.Camera.SetZoom(1.0)
	//cn.Camera.SetZoom(3.0)
	cn.Camera.SetZoom(5.0)

	/*
	ah := rx.NewAxesHelper()
	ah.SetPos(Vec3{10, 10, 0})
	ah.Spawn()
	*/

	var n3 *rx.Node

	var _ = `
	
	/*
	n2 := rx.GenCubeMeshNode(1, 1, 1)
	sce.Add(n2)
	//n2.SetPos(Vec3{5, 5, 10})
	//n2.SetPos(Vec3{0, 0, 100})
	//n2.SetPos(Vec3{2, 0, 0})
	n2.SetPos(Vec3{3, 3, 0})
	//n2.SetRot(Vec3{90, 0, 0})
	//node = n2
	*/

	_ = phys.ShapeTypeBox
	//n2.AddPhys(1000, phys.ShapeTypeBox)

	//n2.Phys.Spawn()

	//n3 := rx.GenCubeMeshNode(1, 1, 1)
	n3 := rx.GenCubeMeshNode(2, 2, 2)
	//n3 := rx.GenCubeMeshNode(4, 4, 4)
	sce.Add(n3)
	//n3.SetPos(Vec3{5, 5, 20})
	//n3.SetPos(Vec3{3, 3, 20})
	//n3.SetPos(Vec3{0, 0, 10})
	//n3.SetPos(Vec3{3, 3, 10})
	n3.SetPos(Vec3{8, 8, 10})
	//n3.AddPhys(0, phys.ShapeTypeBox)
	n3.AddPhys(1, phys.ShapeTypeBox)
	//n3.AddPhys(5000, phys.ShapeTypeBox)
	//n3.AddPhys(5000, phys.ShapeTypeSphere)
	//n3.AddPhys(1, phys.ShapeTypeSphere)

	//n2.Phys.SetColGroup(1)
	//n2.Phys.SetColMask(1)
	//panic(2)

	//n3.Phys.SetColGroup(1)
	//n3.Phys.SetColGroup(0)
	//n3.Phys.SetColMask(0)
	//n3.Phys.SetColMask(-1)
	
	//n3.Phys.SetColMask(0xffff)

	mask := n3.Phys.ColMask()
	println("mask:", mask)

	//panic(2)
	
	pos := n3.Phys.Pos()
	println("pos:", pos)
	
	//n3.Phys.SetPos(Vec3{4, 4, 10})
	
	//panic(2)
	`


	var maskAll uint
	maskAll = 0xffffffff
	_ = maskAll

	// XXX spawn point?
	spawnHeight := 4.0
	_ = spawnHeight

	spawnHeight = 1.2

	var _ = `
	var w1 *rx.Node
	if true {
		//w1 = rx.GenCylinderMeshNode(0.66, 0.66, 0.045, 16) // XXX default
		w1 = rx.GenCylinderMeshNode(0.66, 0.66, 0.5, 16)
		//w1 = rx.GenCylinderMeshNode(4, 4, 0.2, 16)
		//w1 := rx.GenCylinderMeshNode(4, 4, 2, 16)
		sce.Add(w1)
		//w1.SetPos(Vec3{0, 0, 0})
		//w1.SetPos(Vec3{0, 0, 4})
		//w1.SetPos(Vec3{2, 0, 4 - 0.33})
		//w1.SetPos(Vec3{0, 0, 4 - 0.33}) // XXX
		w1.SetPos(Vec3{0, 0, spawnHeight - 0.33})
		//w1.SetRot(Vec3{90, 0, 0})
		//w1.SetRot(Vec3{0, 0, 0})
		//w1.SetRot(Vec3{90, 0, 0})
		w1.SetRot(Vec3{-90, 0, 0})
		//w1.SetRot(Vec3{90, 0, 180}) // XXX fixme: 180 z flip
		//w1.AddPhys(1, phys.ShapeTypeCylinder)
		//w1.AddPhys(0, phys.ShapeTypeCylinder)
		w1.AddPhys(10, phys.ShapeTypeCylinder)
		newRot := w1.Phys.Rot()
		println("newRot:", newRot)
		//panic(2)

		w1.Phys.SetColGroup(ColGroupBike)
		//w1.Phys.SetColMask(maskAll ^ 2)
		w1.Phys.SetColMask(ColMaskEverything ^ ColGroupBike)
		//w1.Phys.SetColMask(0xffff)
		//w1.Phys.SetColMask(-1)
		//panic(2)

		//w1.SetVisible(false)
		//w1.Phys.ClearForces()
		w1.Phys.DisableDeactivation()
	}


	var fork *rx.Node
	if true {
		//fork = rx.GenCubeMeshNode(2, 2, 5)
		fork = rx.GenCubeMeshNode(0.132, 0.56, 0.80)
		sce.Add(fork)
		//fork.SetPos(Vec3{0, 0, 6})
		//fork.SetPos(Vec3{0, 0, 1.5})
		//fork.SetPos(Vec3{0, 0, 4}) // XXX
		fork.SetPos(Vec3{0, 0, spawnHeight})

		mat := rx.NewMaterial("res/materials/base/static.json")
		mat.Name = "field"
		mat.Diffuse = Vec3{.6, .3, .1}
		mat.SetUnshaded(true)

		fork.Mesh.SetMaterial(mat)

		println("mat:", mat)


		fork.AddPhys(1, phys.ShapeTypeBox)
		//fork.AddPhys(0, phys.ShapeTypeBox)

		fork.Phys.SetColGroup(ColGroupBike)
		//fork.Phys.SetColMask(maskAll ^ 2)
		fork.Phys.SetColMask(ColMaskEverything ^ ColGroupBike)
		//fork.Phys.SetColGroup(0)
		//fork.Phys.SetColMask(0)

		fork.SetVisible(false)
		//fork.Phys.ClearForces()
		fork.Phys.DisableDeactivation()
		//panic(2)
	}

	//po1 := w1.Phys.(*bt.PhysObj)

	println("creating hinge...")
	println("bt:", bt.NewHinge)
	println("x1:", w1 == nil, w1)
	println("x:", w1.Phys == nil)
	//h1 := btcpp.NewHinge(w1.Phys, fork.Phys, Vec3Zero, Vec3Zero, Vec3Zero, Vec3Zero, false)
	//h1 := btcpp.NewHinge(po1, fork.Phys, Vec3Zero, Vec3Zero, Vec3Zero, Vec3Zero, false)
	//h1 := bt.NewHinge(w1.Phys, fork.Phys, Vec3Zero, Vec3Zero, Vec3Zero, Vec3Zero, false)
	//h1 := bt.NewHinge(nil, nil, Vec3Zero, Vec3Zero, Vec3Zero, Vec3Zero, false)
	//h1 := bt.NewHinge(fork.Phys, w1.Phys, Vec3Zero, Vec3Zero, Vec3Zero, Vec3Zero)
	//h1 := bt.NewHinge(w1.Phys, fork.Phys, Vec3Zero, Vec3Zero, Vec3Zero, Vec3Zero)
	//h1 := bt.NewHinge(w1.Phys, fork.Phys, Vec3Zero, Vec3{-90, 0, 0}, Vec3{0, 0, -0.4}, Vec3Zero)
	//h1 := bt.NewHinge(w1.Phys, fork.Phys, Vec3Zero, Vec3{-90, 0, 0}, Vec3{0, 0, -0.4}, Vec3{0, -90, 0})
	//h1 := bt.NewHinge(w1.Phys, fork.Phys, Vec3Zero, Vec3{90, -180, 0}, Vec3{0, 0, -0.4}, Vec3{0, 0, 0})
	//h1 := bt.NewHinge(w1.Phys, fork.Phys, Vec3Zero, Vec3{0, 0, 90}, Vec3{0, 0, -0.4}, Vec3{90, 0, 90})
	//h1 := bt.NewHinge(w1.Phys, fork.Phys, Vec3Zero, Vec3{0, 0, 90}, Vec3{0, 0, -0.4}, Vec3{0, 90, 90}) // XXX v0 (x-down)
	h1 := bt.NewHinge(w1.Phys, fork.Phys, Vec3Zero, Vec3{0, 0, 0}, Vec3{0, 0, -0.4}, Vec3{-90, 0, 0}) // XXX v1 (y-down)
	_ = h1
	
	//panic(2)
	`

	
	var fw *Wheel
	var rw *Wheel
	var xframe *Frame
	var xfork *Fork
	var bike *Bike
	if true {

		//xspawnHeight := 1.5
		//xspawnHeight := 1.1
		xspawnHeight := 2.0

		p1 := NewWheelProps()
		p1.diameter = 0.66
		p1.width = 0.2
		p1.mass = 10
		//p1.mass = 2
		p1.friction = 100
		
		
		p1.Verify()
	
		fw = NewWheel(p1)
		fw.spawnPos = Vec3{2, 2, xspawnHeight}
		fw.Init()

		fw.node.SetName("fwheel")
		fw.node.Phys.SetName("fwheel")

		fw.Spawn()

		p2 := NewWheelProps()
		p2.diameter = 0.66
		p2.width = 0.2
		p2.mass = 10
		//p2.mass = 2
		p2.friction = 100

		p2.Verify()

		rw = NewWheel(p2)
		rw.spawnPos = Vec3{0, 2, xspawnHeight}
		//rw.spawnPos = Vec3{10, 2, xspawnHeight}
		rw.Init()

		rw.node.SetName("rwheel")
		rw.node.Phys.SetName("rwheel")
		
		rw.Spawn()

		//t := rw.node.Phys.Friction()
		//println(t)
		//panic(2)

		var frame *rx.Node		
		_ = frame
		/*
		if true {
			l := 4.0
			frame = rx.GenCubeMeshNode(l, 0.6, 0.2)
			sce.Add(frame)
			frame.SetPos(Vec3{l/2., 2, xspawnHeight})
		
			mat := rx.NewMaterial("res/materials/base/static.json")
			mat.Name = "field"
			mat.Diffuse = Vec3{.6, .3, .1}
			mat.SetUnshaded(true)
		
			frame.Mesh.SetMaterial(mat)
		
		
			frame.AddPhys(1, phys.ShapeTypeBox)
		
			frame.Phys.SetColGroup(ColGroupBike)
			//fork.Phys.SetColMask(maskAll ^ 2)
			frame.Phys.SetColMask(ColMaskEverything ^ ColGroupBike)
			//fork.Phys.SetColGroup(0)
			//fork.Phys.SetColMask(0)
		
			//frame.SetVisible(false)
			//fork.Phys.ClearForces()
			frame.Phys.DisableDeactivation()
			//panic(2)
		}
		*/

		// Frame
		//var f1 *Frame
		if true {
			l := 4.0
		
			p3 := NewFrameProps()
			p3.dim = Vec3{l, 0.6, .02}
			p3.mass = 1.0
			//p3.mass = 0.0

			p3.Verify()
		
			xframe = NewFrame(p3)
			//xframe.spawnPos = Vec3{0, 0, 2}
			xframe.spawnPos = Vec3{2, 2, xspawnHeight}
			//xframe.spawnPos = Vec3{4, 2, xspawnHeight}
			xframe.Init()

			xframe.node.SetName("frame")
			xframe.node.Phys.SetName("frame")
			
			xframe.Spawn()

			println(xframe.Pos())
			println(xframe.Rot())

			//panic(2)

			//frame = f1.node
		}

		// Fork
		if true {
			p4 := NewForkProps()
			p4.dim = Vec3{0.3, 0.5, 2}
			p4.mass = 1.0
			//p4.mass = 0.0
		
			p4.Verify()
		
			xfork = NewFork(p4)
			xfork.spawnPos = Vec3{2, 2, xspawnHeight}
			xfork.Init()

			xfork.node.SetName("fork")
			xfork.node.Phys.SetName("fork")
				
			xfork.Spawn()
		}

		println("adding frame wheel links...")

		/*
		//fh := bt.NewHinge(fw.node.Phys, frame.Phys, Vec3Zero, Vec3{0, 0, 0}, Vec3{0, 0, -0.4}, Vec3{-90, 0, 0}) // XXX v1 (y-down)
		fh := bt.NewHinge(fw.node.Phys, frame.Phys, Vec3Zero, Vec3{0, 0, 0}, Vec3{2, 0, 0}, Vec3{-90, 0, 0}) // XXX v1 (y-down)
		//fh := bt.NewHinge(fw.node.Phys, frame.Phys, fw.node.Pos(), Vec3{0, 0, 0}, fw.node.Pos(), Vec3{-90, 0, 0}) // XXX v1 (y-down)
		_ = fh

		//rh := bt.NewHinge(rw.node.Phys, frame.Phys, Vec3Zero, Vec3{0, 0, 0}, Vec3{0, 0, -0.4}, Vec3{-90, 0, 0}) // XXX v1 (y-down)
		rh := bt.NewHinge(rw.node.Phys, frame.Phys, Vec3Zero, Vec3{0, 0, 0}, Vec3{-2, 0, 0}, Vec3{-90, 0, 0}) // XXX v1 (y-down)
		//rh := bt.NewHinge(rw.node.Phys, frame.Phys, rw.node.Pos(), Vec3{0, 0, 0}, rw.node.Pos(), Vec3{-90, 0, 0}) // XXX v1 (y-down)
		_ = rh
		*/
		
		///*
		b := NewBike()
		// ???
		b.fwheel = fw
		b.rwheel = rw
		b.frame = xframe
		b.fork = xfork

		/*

		//b.fwlinkOriginB = Vec3{2, 0, 0}
		b.fwlinkOriginB = Vec3{0, 0, -1}

		//b.rwlinkOriginB = Vec3{-2, 0, 0}

		b.rwlinkOriginB = Vec3{0, 0, 0 }
		//b.rwlinkOriginA = Vec3{-2, 0, 0 }
		//b.rwlinkOriginA = Vec3{-10, 0, 0 }
		b.rwlinkOriginA = Vec3{10, 0, 0 }
		b.rwlinkBasisA = Vec3{0, 0, 0}
		//b.rwlinkBasisB = Vec3{-90, 0, 0}
		b.rwlinkBasisB = Vec3{0, 0, 0}

		b.flinkOriginB = Vec3{1, 0, 0}

		*/

		// Reset
		/*
		b.rwlinkBasisA = Vec3{0, 0, 0}
		b.rwlinkBasisB = Vec3{0, 0, 0}
		b.rwlinkOriginA = Vec3{0, 0, 0 }
		b.rwlinkOriginB = Vec3{0, 0, 0 }
		*/

		//b.rwlinkOriginB = Vec3{-2, 0, 0}
		//b.rwlinkOriginA = Vec3{2, 0, 0}
		//b.rwlinkOriginB = Vec3{2, 0, 0}

		//b.rwlinkOriginA = Vec3{10, 0, 0}
		//b.rwlinkOriginB = Vec3{-10, 0, 0}
		//b.rwlinkOriginB = Vec3{10, 0, 0}

		// XXX bike data

		// rwlink
		// Parent (B)
		b.rwlinkParentOrigin = Vec3{-2, 0, 0}

		// flink
		b.flinkParentOrigin = Vec3{1, 0, 0}

		// fwlink
		b.fwlinkParentOrigin = Vec3{0, 0, -1}


		// Support wheels
		if false {
			ps := NewWheelProps()
			ps.mass = 0.6
			ps.friction = 1.0
			ps.diameter = 0.20
			ps.width = 0.08
			ps.Verify()
		
			lswheel := NewWheel(ps)
			lswheel.Init()
			lswheel.Spawn()

			rswheel := NewWheel(ps)
			rswheel.Init()
			rswheel.Spawn()

			b.lswheel = lswheel
			b.rswheel = rswheel

			b.lswlinkOriginB = Vec3{-2.2, 0.9, -.40}
			b.rswlinkOriginB = Vec3{-2.2, -0.9, -.40}
		}

		
		b.Init()

		b.Spawn()

		bike = b

		// Extra
		b.LockBar(true)
		//b.lockBar(false)
		
		//*/

		b.fwheel.node.Phys.ClearForces()
		b.rwheel.node.Phys.ClearForces()
		b.frame.node.Phys.ClearForces()
		b.fork.node.Phys.ClearForces()
		//b.lswheel.node.Phys.ClearForces()
		//b.rswheel.node.Phys.ClearForces()


		// XXX 2d plane lock
		//b.frame.node.Phys.Test()

		b.frame.node.Phys.SetLinearFactor(Vec3{1, 0, 1})
		b.frame.node.Phys.SetAngularFactor(Vec3{0, 1, 0})

		b.rwheel.node.Phys.SetLinearFactor(Vec3{1, 0, 1})
		b.rwheel.node.Phys.SetAngularFactor(Vec3{0, 1, 0})


		//b.fwlink.EnableAngularMotor(true, 1, 1)
		//b.rwlink.EnableAngularMotor(true, 1, 1)

		//panic(spawnHeight)
		b.SetPos(Vec3{10, 0, spawnHeight})
		//b.setPos(Vec3{10, 0, 10})

		//panic(2)
	}


	xh1 := rx.NewAxesHelper()
	xh1.DrawOriginLink = true
	xh1.DrawOriginLinkColor = Vec3{1, 1, 1}
	xh1.NoDepthTest = true
	xh1.Size = 10
	_ = xh1

	//xh1.Spawn()

	xh2 := rx.NewAxesHelper()
	xh2.DrawOriginLink = true
	xh2.DrawOriginLinkColor = Vec3{1, 0, 0	}
	xh2.NoDepthTest = true
	xh2.Size = 6
	_ = xh2

	//xh2.Spawn()

	for app.Step() {
		//println("step")

		if app.Pause() {
			continue
		}

		dt := app.GetDt()
		_ = dt

		//n2.Phys.Update(dt)
		//_rxi.PhysSys.Update(dt)

		//n3.Phys.Update(dt)

		println("step")
		//panic(2)
		//newPos := n2.Phys.Pos()
		//_ = newPos

		//println("new pos:", newPos)
		
		if n3 != nil {
			s := fmt.Sprintf("pos: %s\nrot: %s", n3.Phys.Pos(), n3.Phys.Rot())
			rx.DrawText(s, 10, 10, 16, Vec3One)
		}

		//s2 := fmt.Sprintf("torque: %.2f\nmax impulse: %.2f", bike.motorTorque, bike.motorMaxImpulse)
		s2 := fmt.Sprintf("max impulse: %.2f\ntorque: %.2f", bike.motorMaxImpulse, bike.motorTorque)
		s2 += fmt.Sprintf("\n\nforward: %v brake: %v", bike.forward, bike.brake)

		s2 += fmt.Sprintf("\n\nrwheel pos: %v rot: %v", bike.rwheel.Pos(), bike.rwheel.Rot())
		s2 += fmt.Sprintf("\nframe pos: %v rot: %v", bike.frame.Pos(), bike.frame.Rot())

		rx.DrawText(s2, 10, 50, 16, Vec3One)

		if false {

			// Test
			rwlinkPosA := bike.frame.Pos().Add(bike.rwlinkChildOrigin)
			//rwlinkPos := bike.frame.pos().Add(bike.rwlinkOriginB)
			xh1.SetPos(rwlinkPosA)
			_ = rwlinkPosA

			rwlinkPosB := bike.frame.Pos().Add(bike.rwlinkParentOrigin)
			xh2.SetPos(rwlinkPosB)
			_ = rwlinkPosB

			//panic(2)
		}

		/*
		kb := inputsys.Keyboard
		velX := 0.0
		velY := 0.0
		velZ := 0.0
		moveSpeed := 4.0
		if kb.IsKeyPressed(glfw.KeyUp) && !bike.forward {
			velY = moveSpeed
		}
		if kb.IsKeyPressed(glfw.KeyDown) {
			velY = -moveSpeed
		}
		if kb.IsKeyPressed(glfw.KeyLeft) {
			velX = -moveSpeed
		}
		if kb.IsKeyPressed(glfw.KeyRight) {
			velX = moveSpeed
		}
		if kb.IsKeyPressed(glfw.KeyA) {
			velZ = moveSpeed
		}
		if kb.IsKeyPressed(glfw.KeyS) {
			velZ = -moveSpeed
		}
		*/

		bike.Update(dt)

		if moveCamera {

			bikePos := bike.Pos()
			newCamPos := initialCamPos.Add(bikePos)

			cn.SetPos(newCamPos)
		}

		app.Flip()
		//if app.Frame() == 2 {
		//if app.Frame() == 3 {
		if app.Frame() == 20 {
			//app.TogglePause()
		}
	}

	rx.TestFwDeinit()

	println("exiting...")
}

