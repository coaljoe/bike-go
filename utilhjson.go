package main

import (
	//"bytes"
	//"fmt"
	"io/ioutil"

	"github.com/hjson/hjson-go"
)

func unmarshalHjsonFromFile(path string) map[string]interface{} {

	content, err := ioutil.ReadFile(path)
	if err != nil {
		panic(err)
	}

	var dat map[string]interface{}

	// Decode and a check for errors.
	if err := hjson.Unmarshal(content, &dat); err != nil {
		panic(err)
	}
	//fmt.Println(dat)

	return dat
}

func hjHasDef(def map[string]interface{}, name string) bool {
	return def[name] != nil
}

func hjGetDef(def map[string]interface{}, name string) map[string]interface{} {
	x := def[name]
	if x != nil {
		return x.(map[string]interface{})
	} else {
		panic("get def fail key=" + name)
	}
}

func hjGetBool(def map[string]interface{}, name string) bool {
	if v := def[name]; v != nil {
		return v.(bool)
	} else {
		panic("read fail key=" + name)
	}
}

var hjGetString = _getString
var hjGetFloat = _getFloat
var hjGetInt = _getInt
