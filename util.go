package main

import (
	"kristallos.ga/lib/debug"
	//"os"
	"fmt"
)

var println = fmt.Println

var p = debug.P
var pp = debug.Pp
var pv = debug.Pv
var pf = debug.Pf
var dump = debug.Dump
var pdump = debug.Pdump
//var __xp = debug.Xp
//var __xpp = debug.Xpp
//var pq = debug.Pq

var __p = p
var _ = __p

var __pp = pp
var _ = __pp
