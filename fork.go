package main

import (
	"kristallos.ga/rx"
	"kristallos.ga/rx/phys"
	. "kristallos.ga/rx/math"
)

type Fork struct {
	*BikeElem

	// Props
	// Size
	dim Vec3
	mass float64

	// Initial pos
	spawnPos Vec3

	spawned bool
}

func NewFork(props ForkProps) *Fork {
	println("NewFork()")

	f := &Fork{
		BikeElem: NewBikeElem(),

		dim: props.dim,
		mass: props.mass,

		// Defaults
		spawnPos: Vec3Zero,
	}
	
	return f
}

func (f *Fork) Init() {
	
	fork := rx.GenCubeMeshNode(f.dim.X(), f.dim.Y(), f.dim.Z())
	ctx.sce.Add(fork)
	//frame.SetPos(Vec3{l/2., 2, xspawnHeight})
			
	fork.Mesh.SetMaterial(ctx.bikeMat)
	
		
	fork.AddPhys(f.mass, phys.ShapeTypeBox)
		
	fork.Phys.SetColGroup(ColGroupBike)
	//fork.Phys.SetColMask(maskAll ^ 2)
	fork.Phys.SetColMask(ColMaskEverything ^ ColGroupBike)
	//fork.Phys.SetColGroup(0)
	//fork.Phys.SetColMask(0)
	
	fork.SetVisible(false)
	//fork.Phys.ClearForces()
	fork.Phys.DisableDeactivation()
	//panic(2)

	f.node = fork
}

func (f *Fork) Spawn() {

	f.spawned = true
}
