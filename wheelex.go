package main

import (
	"kristallos.ga/rx/phys/bt"
)

type WheelProps struct {
	diameter float64
	width float64
	mass float64
	friction float64
}

func NewWheelProps() WheelProps {
	println("NewWheelProps")

	//t := bt.C_default_friction
	//println(t)
	//panic(2)
	
	wp := WheelProps{
		mass: 1.0, // Default mass
		//friction: 0.5, // XXX FIXME bullet default friction?
		friction: bt.C_default_friction, // XXX default btcpp (0.5)
	}

	return wp
}

func (wp WheelProps) Verify() {
	println("wp.Verify()")
	
	if wp.mass <= 0.0 {
		panic("mass cannot be zere")
	}
	if wp.diameter <= 0.0 {
		panic("diameter cannot be zero")
	}
	if wp.width <= 0.0 {
		panic("width cannot be zero")
	}
}
