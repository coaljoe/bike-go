package main

import (
	"kristallos.ga/rx"
	. "kristallos.ga/rx/math"
)

type Context struct {
	rxi *rx.Rx
	sce *rx.Scene

	bikeMat *rx.Material
}

var ctx *Context

func init() {
	println("context init()")

	ctx = &Context{}
}

// XXX should be called after rx?
func initContext() {
	println("initContext()")

	mat := rx.NewMaterial("res/materials/base/static.json")
	mat.Name = "bikeMat"
	mat.Diffuse = Vec3{.6, .3, .1}
	mat.SetUnshaded(true)

	ctx.bikeMat = mat
}
