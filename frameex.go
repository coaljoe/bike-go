package main

import (
	. "kristallos.ga/rx/math"
)

type FrameProps struct {
	dim Vec3 // sx, sy, sz
	mass float64
}

func  NewFrameProps() FrameProps {
	println("NewFrameProps()")
	
	fp := FrameProps{
		mass: 1.0, // Default mass
	}

	return fp
}

func (fp FrameProps) Verify() {
	println("fp.Verify()")
	
	if fp.mass < 0 {
		panic("mass cannot be negative")
	}
	if fp.dim == Vec3Zero || 
		(fp.dim.X() < 0 || fp.dim.Y() < 0 || fp.dim.Z() < 0) {
		panic("height cannot be zero")
	}
}
