package main

import (
	"github.com/go-gl/glfw/v3.3/glfw"

	ps "kristallos.ga/lib/pubsub"
	"kristallos.ga/rx"
	"kristallos.ga/rx/phys/bt"
	. "kristallos.ga/rx/math"
)

type Bike struct {
	fwheel *Wheel
	rwheel *Wheel
	frame *Frame
	fork *Fork

	// fwheel link (fwheel-fork)
	// Hinge transform
	fwlinkChildOrigin Vec3 // A: fwheel
	fwlinkChildBasis Vec3
	fwlinkParentOrigin Vec3 // B: frame
	fwlinkParentBasis Vec3
	fwlink *bt.Hinge

	// rwheel link (rwheel-frame)
	// Hinge transform
	rwlinkChildOrigin Vec3 // A: rwheel
	rwlinkChildBasis Vec3
	rwlinkParentOrigin Vec3 // B: frame
	rwlinkParentBasis Vec3
	rwlink *bt.Hinge

	// fork link (fork-frame)
	// Hinge transform
	flinkChildOrigin Vec3 // A: fork
	flinkChildBasis Vec3
	flinkParentOrigin Vec3 // B: frame
	flinkParentBasis Vec3
	flink *bt.Hinge

	// Support wheels
	lswheel *Wheel
	rswheel *Wheel

	// left support wheel link (lswheel-frame)
	// Hinge transform
	lswlinkOriginA Vec3 // A: lswheel
	lswlinkBasisA Vec3
	lswlinkOriginB Vec3 // B: frame
	lswlinkBasisB Vec3
	lswlink *bt.Hinge

	// right support wheel link (rswheel-frame)
	// Hinge transform
	rswlinkOriginA Vec3 // A: rswheel
	rswlinkBasisA Vec3
	rswlinkOriginB Vec3 // B: frame
	rswlinkBasisB Vec3
	rswlink *bt.Hinge

	// State
	forward bool
	brake bool
	steer bool
	steerDir int

	// Param for target_velocity (?)
	motorTorque float64
	motorTorqueMin float64
	motorTorqueMax float64
	motorTorqueInc float64

	// Param for max_impulse (?)
	motorMaxImpulse float64

	forkRotateMin float64
	forkRotateMax float64

	spawned bool
}

func NewBike() *Bike {
	println("NewBike(")

	b := &Bike{
		// XXX Default
		fwlinkParentBasis: Vec3{-90, 0, 0}, // y-down
		rwlinkParentBasis: Vec3{-90, 0, 0}, // y-down
		motorTorqueMin: 2,
		//motorTorqueMax: 34,
		//motorTorqueMax: 1000,
		//motorTorqueMax: 20,
		motorTorqueMax: 12,
		//motorTorqueInc: 50,
		//motorTorqueInc: 1000,
		motorTorqueInc: 20,
		//motorMaxImpulse: 1.0,
		//motorMaxImpulse: 3.0,
		motorMaxImpulse: 2.8,
		forkRotateMin: 0.2,
		forkRotateMax: 2.0,

		// Support wheels
		lswlinkBasisB: Vec3{-90, 0, 0}, // y-down
		rswlinkBasisB: Vec3{-90, 0, 0}, // y-down
	}

	sub(rx.Ev_key_press, b.onKeyPress)
	sub(rx.Ev_key_release, b.onKeyRelease)
	return b
}

func (b *Bike) onKeyPress(ev *ps.Event) {
	key := ev.Data.(glfw.Key)
	if key == glfw.KeyUp && !b.forward {
		//panic("derp")
		b.StartForward()
	}
	if key == glfw.KeyDown && !b.brake {
			//panic("derp")
			b.StartBrake()
	}
	if key == glfw.KeyLeft && !b.steer {
		//panic("derp")
		b.StartSteer(-1)
	}
	if key == glfw.KeyRight && !b.steer {
		//panic("derp")
		b.StartSteer(+1)
	}

	if key == glfw.KeySpace {
		b.Flip()
		//panic(2)
	}
}

func (b *Bike) onKeyRelease(ev *ps.Event) {
	key := ev.Data.(glfw.Key)
	if key == glfw.KeyUp && b.forward {
		//panic("derp2")
		b.StopForward()
	}
	if key == glfw.KeyDown && b.brake {
		//panic("derp")
		b.StopBrake()
	}
	if key == glfw.KeyLeft && b.steer {
		//panic("derp")
		b.StopSteer()
	}
	if key == glfw.KeyRight && b.steer {
		//panic("derp")
		b.StopSteer()
	}
}

func (b *Bike) Init() {
	println("Bike Init")

	// XXX dont forget to specify hinge angles
	// (hinge basises should not be all zero)
	// XXX TODO: add zero basises checks?
	println("creating links...")

	///*
	// Front wheel
	println()
	println("creating fwlink...")
	//fh := bt.NewHinge(fw.node.Phys, frame.Phys, Vec3Zero, Vec3{0, 0, 0}, Vec3{2, 0, 0}, Vec3{-90, 0, 0}) // XXX v1 (y-down)
	/*
	fwlink := bt.NewHinge(b.fwheel.node.Phys, b.fork.node.Phys,
		b.fwlinkOriginA, b.fwlinkBasisA, b.fwlinkOriginB, b.fwlinkBasisB)
	*/
	fwlink := bt.NewHinge(b.fwheel.node.Phys, b.fork.node.Phys, // child, parent
		b.fwlinkChildOrigin, b.fwlinkChildBasis,	// child (A)
		b.fwlinkParentOrigin, b.fwlinkParentBasis)	// parent (B)
	b.fwlink = fwlink
	//*/

	// Rear wheel
	println()
	println("creating rwlink...")
	//rh := bt.NewHinge(rw.node.Phys, frame.Phys, Vec3Zero, Vec3{0, 0, 0}, Vec3{-2, 0, 0}, Vec3{-90, 0, 0}) // XXX v1 (y-down)
	/*
	rwlink := bt.NewHinge(b.rwheel.node.Phys, b.frame.node.Phys, // child, parent
		b.rwlinkOriginA, b.rwlinkBasisA, // child (A)
		b.rwlinkOriginB, b.rwlinkBasisB) // parent (B)
	*/
	//rwlink := bt.NewHinge(b.frame.node.Phys, b.rwheel.node.Phys,
	//	b.rwlinkOriginA, b.rwlinkBasisA, b.rwlinkOriginB, b.rwlinkBasisB)
	rwlink := bt.NewHinge(b.rwheel.node.Phys, b.frame.node.Phys, // child, parent
		b.rwlinkChildOrigin, b.rwlinkChildBasis,    // child (A)
		b.rwlinkParentOrigin, b.rwlinkParentBasis)  // parent (B)
	b.rwlink = rwlink

	//println("f:", rwlink.Friction())
	println()
	//panic(2)

	// Fork
	/*
	flink := bt.NewHinge(b.fork.node.Phys, b.frame.node.Phys,
		b.flinkOriginA, b.flinkBasisA, b.flinkOriginB, b.flinkBasisB)
	*/
	flink := bt.NewHinge(b.fork.node.Phys, b.frame.node.Phys, // child, parent
		b.flinkChildOrigin, b.flinkChildBasis,		// child (A)
		b.flinkParentOrigin, b.flinkParentBasis)	// parent (B)
	b.flink = flink

	// Support wheels
	if b.lswheel != nil {
		lswlink := bt.NewHinge(b.lswheel.node.Phys, b.frame.node.Phys,
			b.lswlinkOriginA, b.lswlinkBasisA, b.lswlinkOriginB, b.lswlinkBasisB)
		b.lswlink = lswlink
	}
	if b.rswheel != nil {
		rswlink := bt.NewHinge(b.rswheel.node.Phys, b.frame.node.Phys,
			b.rswlinkOriginA, b.rswlinkBasisA, b.rswlinkOriginB, b.rswlinkBasisB)
		b.rswlink = rswlink
	}
}

func (b *Bike) LockBar(v bool) {
	println("bike LockFork() v:", v)

	b.flink.Lock(v)
}

func (b *Bike) Speed() float64 {
	ret := b.frame.node.Phys.RelLinearVelocity().X()

	return ret
}

func (b *Bike) StartForward() {
	println("bike StartForward()")

	// Clamp
	if b.motorTorque < b.motorTorqueMin {
		b.motorTorque = b.motorTorqueMin
	}

	//b.rwlink.EnableMotor(true)
	//panic(2)

	b.forward = true
}

func (b *Bike) StopForward() {
	println("bike StopForward()")

	// Clamp
	if b.motorTorque > b.motorTorqueMin {
		b.motorTorque = b.motorTorqueMin // XXX?
	}

	b.rwlink.EnableMotor(false)

	b.forward = false
}

func (b *Bike) StartBrake() {
	println("bike StartBrake()")

	//b.rwlink.SetFriction(10000000.0)
	//b.rwlink.SetFriction(1000.0)
	//b.rwlink.SetFriction(1.0) // XXX add value/param to bike
	b.rwlink.SetFriction(2.0)

	//panic(2)

	b.brake = true
}

func (b *Bike) StopBrake() {
	println("bike StoptBrake()")

	//b.rwlink.SetFriction(0.0)
	b.rwlink.SetFriction(0.01) // XXX fixme? add as default value/param to bike?

	b.brake = false
}

// XXX fixme
// -1 = left
// +1 = right
func (b *Bike) StartSteer(dir int) {
	println("bike StartSteer()")

	//panic(2)

	b.LockBar(false)

	b.steerDir = dir
	b.steer = true
}

func (b *Bike) StopSteer() {
	println("bike StopSteer()")

	b.flink.EnableMotor(false)
	b.LockBar(true)

	b.steerDir = 0
	b.steer = false
}

func (b *Bike) Flip() {
	println("bike Flip")

	b.ClearForces()

	pos := b.Pos()
	{
		var o *rx.Node
		var newPos Vec3

		// fwheel
		o = b.fwheel.node
		newPos = pos.Add(b.fwlinkChildOrigin.Negate())
		o.SetPos(newPos)
		//o.SetRotZ(o.RotZ() + 180.0)
		//o.SetRotZ(UnwrapAngle(o.RotZ() + 180.0))

		//b.fwheel.node.RotateX // XXX will not work with phys?

		// rwheel
		o = b.rwheel.node
		newPos = pos.Add(b.rwlinkChildOrigin.Negate())
		o.SetPos(newPos)

		// frame
		//

		// fork
		o = b.fork.node
		newPos = pos.Add(b.flinkChildOrigin.Negate())
		o.SetPos(newPos)

	}

	// Rotate all nodes
	for _, be := range b.Elems() {
		n := be.Node()
		n.SetRotZ(n.RotZ() + 180.0)

		/*
		// XXX use Phys directly
		curRot := n.Rot()
		newRot := curRot
		newRot.SetZ(curRot.Z() + 180.0)
		n.Phys.SetRot(newRot)
		*/
	}

	b.ClearForces()
}

func (b *Bike) Pos() Vec3 {
	return b.frame.node.Pos() // XXX use center of mass position instead? fixme?
}

func (b *Bike) SetPos(p Vec3) {
	b.frame.SetPos(p)
	b.fork.SetPos(b.flinkParentOrigin.Add(p))
	b.fwheel.SetPos(b.fwlinkParentOrigin.Add(p))
	b.rwheel.SetPos(b.rwlinkParentOrigin.Add(p))

	// XXX TODO: Add support wheels
}

func (b *Bike) ClearForces() {
	b.fwheel.node.Phys.ClearForces()
	b.rwheel.node.Phys.ClearForces()
	b.frame.node.Phys.ClearForces()
	b.fork.node.Phys.ClearForces()
}

// Get bike elems
func (b *Bike) Elems() []BikeElemI {
	ret := []BikeElemI{
		b.fwheel,
		b.rwheel,
		b.frame,
		b.fork,
		// XXX add support wheels?		
	}

	return ret
}

func (b *Bike) Spawn() {

	b.spawned = true
}

func (b *Bike) Update(dt float64) {
	println("bike update()")

	if b.forward {
		if false {
	
			//inc := 50.0 * dt
			inc := b.motorTorqueInc * dt
			newV := b.motorTorque + inc
			// Clamp
			if newV > b.motorTorqueMax {
				newV = b.motorTorqueMax
			}
			b.motorTorque = newV

		}

		b.motorTorque = b.motorTorqueMax

		//println("ZZZ:", b.motorTorque)
		
		//b.rwlink.RotateToAngle(b.motorTorque, dt)
		//b.rwlink.EnableAngularMotor(true, b.motorTorque, 10)
		//b.rwlink.EnableAngularMotor(true, b.motorTorque, 1)
		//b.rwlink.EnableAngularMotor(true, b.motorTorque, 5)
		//b.rwlink.EnableAngularMotor(true, b.motorTorque, 10)

		b.rwlink.EnableAngularMotor(true, b.motorTorque, b.motorMaxImpulse)
	}

	if b.brake {
		b.motorTorque = 0
		//b.rwlink.EnableMotor(true)
	}

	if b.steer {
		smax := 6.0
		smin := 1.5
		a, b_, t := b.forkRotateMin, b.forkRotateMax, max(min((b.Speed() - smin) / smax, 1), 0)
		ang := Lerp(a, b_, 1-t)
		if b.steerDir >= 0 {
			ang = -ang
		}
		//println(b.steerDir, ang, b.motorTorqueProgress())
		b.flink.RotateAngular(ang, 0.1)
	}

}
